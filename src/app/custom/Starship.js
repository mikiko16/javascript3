export default class Starship {
    constructor(name, _consumables, _passengers) {
      this.name = name;
      this._consumables = _consumables;
      this._passengers = _passengers;
    }

    get maxDaysInSpace() {
        let consumable = this._consumables.substring(0, this._consumables.lastIndexOf(' '));

        return (consumable / this._passengers);
    }
}