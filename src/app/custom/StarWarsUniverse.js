import Starship from './Starship';

export default class StarWarsUniverse {
  
    constructor() {
      
        this.starships = [];
    }

    async init() {

        await this._getStarshipCount();

        await this.createStarship();
    }

    get theBestStarship() {

        let bestShip =  this.starships.reduce((p, c) => p.maxDaysInSpace > c.maxDaysInSpace ? p : c);
        return bestShip;
    }

    async _getStarshipCount() {
        let count;

        var opts = {
            method: 'GET',      
            headers: {}
            };
            
        await fetch('https://swapi.booost.bg/api/starships/', opts).
            then(function (response) {
                return response.json();
            })
            .then((body) => {
                count = body.count;
        });

        return count;
    }

    async createStarship() {
        
        var opts = {
            method: 'GET',      
            headers: {}
            };
            
        for(let i = 1; i <= 36; i++) {
            await fetch('https://swapi.booost.bg/api/starships/' + i.toString(), opts)
            .then(function (response) {
                return response.json();
                })
            .then(async (body) => {

                if(await this._validateData(body)){
                    let starName = body.name;
                    let starConsumables = body.consumables;
                    let starPassengers = body.passengers;

                    let newStarship = new Starship(starName, starConsumables, starPassengers);

                    let pasrsedStarship = await this.parseStarship(newStarship);

                    this.starships.push(pasrsedStarship);
                }
            })  
            .catch((err) =>
            {
            });
        }
    }

    async _validateData(starship) {
        if (starship.consumables === undefined || 
           starship.consumables === null ||
           starship.consumables === 'unknown') {
               return false;
           }
        
        if (starship.passengers === undefined || 
            starship.passengers === null || 
            starship.passengers === 'n/a' || 
            starship.passengers === '0' ) {
                return false;
            }
        
        return true;
    }

    async parseStarship(starship) {       
        let consumable = starship._consumables.substring(starship._consumables.lastIndexOf(' ') + 1)
        let passenger = starship._passengers;

        if(consumable === 'years' || consumable === 'year') { 
            let number = starship._consumables.substring(0, starship._consumables.lastIndexOf(' '));
            consumable = number * 365

            starship._consumables = consumable + ' days';
        }
        else if (consumable === 'months' || consumable === 'month') {
            let number = starship._consumables.substring(0, starship._consumables.lastIndexOf(' '));
            consumable = number * 30

            starship._consumables = consumable + ' days';
        }
        else if (consumable === 'weeks') {
            let number = starship._consumables.substring(0, starship._consumables.lastIndexOf(' '));
            consumable = number * 7

            starship._consumables = consumable + ' days';
        }

        if (passenger.indexOf(',') > -1)
        { 
            starship._passengers = starship._passengers.replace(",", "");
        }

        return starship;
    }
}